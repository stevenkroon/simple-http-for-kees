require 'sinatra'

get '/' do
  status 200
  'read app.rb for more info'
end

get '/get-success' do
  status 200
  'ok return message here'
end

get '/get-failed' do
  status 500
  'error return message here'
end

post '/post-success' do
  puts params
  status 200
  'ok return message here'
end

post '/post-failed' do
  status 500
  'error return message here'
end

